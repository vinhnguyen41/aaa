from odoo import api, fields, models

class SisDesigningProgram(models.Model):
    # Model arguments
    _name = 'sis.designing.program'

    # Simple fields
    name = fields.Char(string='Name', required=True)
    description = fields.Text(string="Description")

    # Relation fields
    subject_ids = fields.One2many('sis.designing.subject', 'program_id', string='Subject Name')


