from odoo import models, api, fields

class SisDesigningSubject(models.Model):

    # Model arguments
    _name = 'sis.designing.subject'
    _sql_constraints = [('sequence', 'unique(sequence)', 'Sequence already exists !')]

    # Simple fields
    code = fields.Char(string='Code')
    name = fields.Char(string='Name', required=True)
    short_name = fields.Char(string='Short Name')
    sequence = fields.Integer(string='Sequence', required=True)

    # Relation fields
    program_id = fields.Many2one('sis.designing.program', string='Program Name')
    lesson_ids = fields.One2many('sis.designing.lesson', 'subject_id', string='lessons')

    # Get program_id value for "Filter subject by program"
    def _get_default_program(self, program_id):
        sis_program = self.env['sis.designing.program']
        default_program = sis_program.search([('id', '=', program_id)], limit=1)
        return default_program
