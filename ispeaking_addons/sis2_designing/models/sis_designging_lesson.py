from odoo import models, api, fields

class SisDesigningLesson(models.Model):
    # Model arguments
    _name = 'sis.designing.lesson'

    # Simple fields
    title = fields.Char(string='Title')
    lesson_no = fields.Integer(string="No.")
    content = fields.Char(string='Content')
    url_document = fields.Binary(string='Up Load')
    note = fields.Html(string='Lecture Note')

    # Relation fields
    subject_id = fields.Many2one('sis.designing.subject', string='Subject')

    # Get subject_id value for "Filter lesson by subject"
    def _get_default_subject(self, subject_id):
        sis_subject = self.env['sis.designing.subject']
        default_subject = sis_subject.search([('id', '=', subject_id)], limit=1)
        return default_subject
