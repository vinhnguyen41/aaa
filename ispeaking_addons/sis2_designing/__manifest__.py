# Copyright 2019
{
    'name': 'SIS - Designing Module',
    'description': """Once of the modules of the SIS system""",
    'version': '2.0',
    'author': 'Ispeaking IT Department',
    'license': '',
    'category': '',
    'website': 'https://ispeaking.org',
    'images': [],
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/sis_designing_lesson.xml',
        'views/sis_designing_subject.xml',
        'views/sis_designing_program.xml',
        'views/sis_designing_menuitem.xml',
    ],
    'demo': [],
    'installable': True,
}
