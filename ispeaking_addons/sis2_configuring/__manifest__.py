# Copyright 2019
{'name': 'SIS - Configuring',
 'version': '2.0',
 'author': 'iSp team',
 'sequence': 1,
 'website': 'https://ispeaking.org',
 'depends': [],
 'data': [
  'security/ir.model.access.csv',
  'views/sis_configure_room.xml',
  'views/sis_configure_center.xml',
  'views/sis_configure_zone.xml',
  'views/sis_configure_menu.xml',
 ],
 'demo': [],
 'installable': True,
}
