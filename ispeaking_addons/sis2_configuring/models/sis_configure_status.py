from odoo import models, fields, api


class SisConfigureStatus(models.Model):
    _name = "sis.configure.status"
    _description = "Configure Status"

    status_code = fields.Selection(
        [('waiting for class', 'Waiting For Class'), ('studying', 'Studying'), ('sustention', 'Sustention'), ('ended', 'Ended')],
        string="Status Code",
        required=True,
        translate=True,
        default='waiting for class'
    )
