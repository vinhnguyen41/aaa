from odoo import models, fields, api


class SisConfigureCenter(models.Model):
    _name = "sis.configure.center"
    _description = "Configure Center"

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            return {'domain': {'state_id': [('country_id', '=', self.country_id.id)]}}
        else:
            return {'domain': {'state_id': []}}

    @api.onchange('state_id')
    def _onchange_state(self):
        if self.state_id.country_id:
            self.country_id = self.state_id.country_id

    room_ids = fields.One2many('sis.configure.room', 'center_id', string="Room", translate=True, required=True)
    zone_id = fields.Many2one('sis.configure.zone', string="Zone")
    country_id = fields.Many2one('res.country', string="Country", translate=True)
    state_id = fields.Many2one('res.country.state', string="State", translate=True)
    name = fields.Char(string="Center Name", translate=True, required=True)
    address = fields.Char(string="Address", translate=True)
    tel = fields.Char(string="Tel", translate=True)
    email = fields.Char(string="Email", translate=True)
    short_name = fields.Char(string="Short Name", translate=True)
    zone_ids = fields.Many2many('sis.configure.zone', string="Zone IDs", translate=True)
