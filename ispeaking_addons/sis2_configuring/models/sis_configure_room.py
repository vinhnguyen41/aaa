from odoo import models, fields, api


class SisConfigureRoom(models.Model):
    _name = "sis.configure.room"
    _description = "Configure Room"

    center_id = fields.Many2one('sis.configure.center', string="Center")
    name = fields.Char(string="Room Name", translate=True, required=True)
    room_no = fields.Char(string="Room No", translate=True)
    capacity = fields.Integer(string="Capacity", translate=True)
    type = fields.Char(string="Type", translate=True)
    description = fields.Char(string="Description", translate=True)
