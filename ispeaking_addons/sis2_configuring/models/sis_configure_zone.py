from odoo import models, fields, api

class SisConfigureZone(models.Model):
    _name = "sis.configure.zone"
    _description = "Configure Zone"

    name = fields.Char(string="Zone Name", required=True, translate=True)
    center_ids = fields.One2many('sis.configure.center', 'zone_id', string="Center", translate=True, required=True)
