# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'SIS 2.0',
    'version': '1.0',
    'sequence': 5,
    'summary': 'Ispeaking - Sis 2.0',
    'description': "",
    'depends': ['sis2_configuring', 'sis2_designing', 'sis2_employee'],
    'data': [
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': True,
    'auto_install': False
}
