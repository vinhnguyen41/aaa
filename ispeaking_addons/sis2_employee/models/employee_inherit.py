# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools
import odoo.addons

import logging
import sys
_logger = logging.getLogger(__name__)

class SisEmployeeType(models.Model):
    _name = 'sis.employee.type'
    _description = 'employee type'

    code = fields.Char()
    name = fields.Char()

class SisEmployeeInherit(models.Model):
    _inherit = 'hr.employee'
    _description = 'inherit model hr.employee'

    employee_code = fields.Char()
    last_middle_name = fields.Char()
    first_name = fields.Char()
    employee_type = fields.Many2one('sis.employee.type')
    date_ID_number = fields.Char()
    location_ID_number = fields.Char()
    personal_email = fields.Char()
    #permit_no character varying, -- Work Permit No

    # Thông tin khác
    # Địa chỉ
    permanent_address = fields.Char(string="Địa chỉ thường trú")
    current_address = fields.Char(string="Địa chỉ hiện tại")
    # Đào tạo
    name_university = fields.Char(string="Tên trường đào tạo cao nhất")
    type_graduation = fields.Char(string="Bằng tốt nghiệp loại")
    highest_degree = fields.Char(string="Bằng cao nhất")
    year_graduation = fields.Char(string="Năm tốt nghiệp")
    major = fields.Char(string="Chuyển ngành đào tạo")
    major_type = fields.Char(string="Hệ đào tạo")
    # BHXH
    number_BHXH = fields.Char(string="Số bảo hiểm xã hội")
    month_BHXH = fields.Char(string="Tháng đóng BHXH")
    status_BHXH = fields.Char(string="Tình trạng BHXH")
    # Ngân hàng
    #bank_account_id integer, -- Bank Account Number
    number_bank = fields.Char(string="Số tài khoảng ngân hàng")
    name_bank = fields.Char(string="Ngân hàng")
    branch_bank = fields.Char(string="Chi nhánh ngân hàng")
    # Mã số thuế
    tax_number = fields.Char(string="Mã số thuế")
    # Mối quan hệ
    #...


    _sql_constraints = [('work_email', 'unique (work_email)', 'Work email already exists !'),
                        ('employee_code', 'unique (employee_code)', 'Employee code already exists !')]
