# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'SIS - Staff',
    'version': '1.0',
    'sequence': 5,
    'summary': 'Ispeaking - Sis 2.0',
    'description': "",
    'depends': ['base_setup', 'hr'],
    'data': [
        'views/hr_views.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False
}
