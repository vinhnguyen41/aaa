# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * web_dashboard
# 
# Translators:
# Martin Trigaux, 2018
# Marius Stedjan <marius@stedjan.com>, 2018
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-02 10:32+0000\n"
"PO-Revision-Date: 2018-08-24 11:49+0000\n"
"Last-Translator: Marius Stedjan <marius@stedjan.com>, 2018\n"
"Language-Team: Norwegian Bokmål (https://www.transifex.com/odoo/teams/41243/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/js/dashboard_controller.js:60
#, python-format
msgid "%s Analysis"
msgstr ""

#. module: web_dashboard
#: model:ir.model,name:web_dashboard.model_ir_actions_act_window_view
msgid "Action Window View"
msgstr ""

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Activity"
msgstr "Aktivitet"

#. module: web_dashboard
#: model:ir.model,name:web_dashboard.model_base
msgid "Base"
msgstr "Base"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Calendar"
msgstr "Kalender"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Cohort"
msgstr ""

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:65
#, python-format
msgid "Column:"
msgstr ""

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/js/dashboard_view.js:27
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
#, python-format
msgid "Dashboard"
msgstr "Dashbord"

#. module: web_dashboard
#: selection:ir.ui.view,type:0
msgid "Diagram"
msgstr "Diagram"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:40
#, python-format
msgid "Domain Label:"
msgstr ""

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:35
#, python-format
msgid "Domain:"
msgstr "Domene:"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:26
#, python-format
msgid "Field:"
msgstr "Felt:"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Form"
msgstr "Skjema"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Gantt"
msgstr "Gantt"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Graph"
msgstr "Graf"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Grid"
msgstr "Rutenett"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:45
#, python-format
msgid "Group Operator:"
msgstr ""

#. module: web_dashboard
#: model:ir.model,name:web_dashboard.model_ir_http
msgid "HTTP Routing"
msgstr "HTTP-ruting"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:61
#, python-format
msgid "Help:"
msgstr ""

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Kanban"
msgstr "Kanban"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:53
#, python-format
msgid "Modifiers:"
msgstr "Modifikatorer:"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:13
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:18
#, python-format
msgid "Name:"
msgstr "Navn:"

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Pivot"
msgstr "Pivot"

#. module: web_dashboard
#: selection:ir.ui.view,type:0
msgid "QWeb"
msgstr "QWeb"

#. module: web_dashboard
#: selection:ir.ui.view,type:0
msgid "Search"
msgstr "Søk"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:22
#, python-format
msgid "String:"
msgstr ""

#. module: web_dashboard
#: selection:ir.actions.act_window.view,view_mode:0
#: selection:ir.ui.view,type:0
msgid "Tree"
msgstr "Tre"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:57
#, python-format
msgid "Value Label:"
msgstr ""

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:30
#, python-format
msgid "Value:"
msgstr ""

#. module: web_dashboard
#: model:ir.model,name:web_dashboard.model_ir_ui_view
msgid "View"
msgstr "Visning"

#. module: web_dashboard
#: model:ir.model.fields,field_description:web_dashboard.field_ir_actions_act_window_view__view_mode
#: model:ir.model.fields,field_description:web_dashboard.field_ir_ui_view__type
msgid "View Type"
msgstr "Se type"

#. module: web_dashboard
#. openerp-web
#: code:addons/web_dashboard/static/src/xml/dashboard.xml:49
#, python-format
msgid "Widget:"
msgstr "Widget:"
