# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * mrp_account
# 
# Translators:
# Martin Trigaux, 2018
# Luc Muller <lmuller@ityou.de>, 2018
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-19 09:12+0000\n"
"PO-Revision-Date: 2018-08-24 11:38+0000\n"
"Last-Translator: Luc Muller <lmuller@ityou.de>, 2018\n"
"Language-Team: German (https://www.transifex.com/odoo/teams/41243/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid ", from"
msgstr ", von"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Code</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Cost/hour</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Operation</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Operator</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Quantity</span>"
msgstr "<span>Menge</span>"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Raw Materials</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Scraps</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Total Cost</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Unit Cost</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<span>Working Time</span>"
msgstr ""

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<strong>Total Cost of Operations</strong>"
msgstr "<strong>Total Betriebskosten</strong>"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<strong>Total Cost of Raw Materials</strong>"
msgstr "<strong>Total Rohstoffkosten</strong>"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<strong>Total Cost of Scraps</strong>"
msgstr "<strong>Total Ausschusskosten</strong>"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "<strong>Unit Cost</strong>"
msgstr "<strong>Stückkosten</strong>"

#. module: mrp_account
#: model:ir.model.fields,field_description:mrp_account.field_mrp_workcenter__costs_hour_account_id
msgid "Analytic Account"
msgstr "Analysekonto"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "By product(s)"
msgstr ""

#. module: mrp_account
#: model:ir.actions.report,name:mrp_account.action_cost_struct_mrp_production
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_production_form_inherit_view6
#: model_terms:ir.ui.view,arch_db:mrp_account.product_product_inherit_form_view_cost_structure
msgid "Cost Analysis"
msgstr "Kostenauswertung"

#. module: mrp_account
#: model:ir.model.fields,field_description:mrp_account.field_mrp_workcenter_productivity__cost_already_recorded
msgid "Cost Recorded"
msgstr "Kostenauszeichnung"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "Cost Structure"
msgstr "Kostenstruktur"

#. module: mrp_account
#: model:ir.actions.report,name:mrp_account.action_cost_struct_product_template
msgid "Cost Structure Analysis"
msgstr "Kostenstrukturauswertung"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "Cost for"
msgstr "Kosten für"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "Cost of Operations"
msgstr "Betriebskosten"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "Cost of Scraps"
msgstr "Ausschusskosten"

#. module: mrp_account
#: model:account.analytic.account,name:mrp_account.account_assembly_cycle
msgid "Costing Account For Cycle of Assembly."
msgstr ""

#. module: mrp_account
#: model:account.analytic.account,name:mrp_account.account_assembly_hours
msgid "Costing Account For Hours of Assembly."
msgstr ""

#. module: mrp_account
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_mrp_cost_structure__display_name
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_product_template_cost_structure__display_name
msgid "Display Name"
msgstr "Anzeigename"

#. module: mrp_account
#: model:ir.model.fields,help:mrp_account.field_mrp_workcenter__costs_hour_account_id
msgid ""
"Fill this only if you want automatic analytic accounting entries on "
"production orders."
msgstr ""
"Nur ausfüllen, wenn Sie automatische Kostenbuchungen für Fertigungsaufträge "
"erzeugen möchten."

#. module: mrp_account
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_mrp_cost_structure__id
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_product_template_cost_structure__id
msgid "ID"
msgstr "ID"

#. module: mrp_account
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_mrp_cost_structure____last_update
#: model:ir.model.fields,field_description:mrp_account.field_report_mrp_account_product_template_cost_structure____last_update
msgid "Last Modified on"
msgstr "Zuletzt geändert am"

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_report_mrp_account_mrp_cost_structure
msgid "MRP Cost Structure Report"
msgstr ""

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_product_product
msgid "Product"
msgstr "Produkt"

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_product_template
msgid "Product Template"
msgstr "Produktvorlage"

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_report_mrp_account_product_template_cost_structure
msgid "Product Template Cost Structure Report"
msgstr ""

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_mrp_production
msgid "Production Order"
msgstr "Fertigungsauftrag"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "Some of the Manufacturing Order(s) selected are not done yet"
msgstr ""
"Einige der ausgewählten Fertigungsauftrag (-aufträge) sind noch nicht fertig"

#. module: mrp_account
#: model:ir.model.fields,help:mrp_account.field_mrp_workcenter_productivity__cost_already_recorded
msgid ""
"Technical field automatically checked when a ongoing production posts "
"journal entries for its costs. This way, we can record one production's cost"
" multiple times and only consider new entries in the work centers time "
"lines."
msgstr ""
"Das technische Feld wird automatisch überprüft, wenn eine laufende "
"Produktion Buchungen für dessen Kosten quittiert. Auf diese Weise können wir"
" die Kosten einer Produktion mehrfach aufzeichnen und nur neue Buchungen in "
"den Zeitplänen der Arbeitsplätze berücksichtigen."

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_mrp_workcenter
msgid "Work Center"
msgstr "Arbeitsplatz"

#. module: mrp_account
#: model:ir.model,name:mrp_account.model_mrp_workcenter_productivity
msgid "Workcenter Productivity Log"
msgstr "Arbeitsplatzproduktivitätsprotokoll"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "hours"
msgstr "Stunden"

#. module: mrp_account
#: model_terms:ir.ui.view,arch_db:mrp_account.mrp_cost_structure
msgid "manufacturing order(s)."
msgstr "Fertigungsauftrag (-aufträge)"
