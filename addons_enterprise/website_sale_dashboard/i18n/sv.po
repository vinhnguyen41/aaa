# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * website_sale_dashboard
# 
# Translators:
# Kristoffer Grundström <kristoffer.grundstrom1983@gmail.com>, 2018
# Martin Trigaux, 2018
# Anders Wallenquist <anders.wallenquist@vertel.se>, 2018
# Martin Wilderoth <martin.wilderoth@linserv.se>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~11.3+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-25 08:34+0000\n"
"PO-Revision-Date: 2018-06-25 08:34+0000\n"
"Last-Translator: Martin Wilderoth <martin.wilderoth@linserv.se>, 2018\n"
"Language-Team: Swedish (https://www.transifex.com/odoo/teams/41243/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: website_sale_dashboard
#: model:ir.model.fields,field_description:website_sale_dashboard.field_sale_report__is_abandoned_cart
msgid "Abandoned Cart"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "Abandoned Carts"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "At A Glance"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "Average Untaxed Total"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.res_config_settings_view_form
msgid "Client ID"
msgstr "Klient-ID"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.res_config_settings_view_form
msgid "Client Secret"
msgstr "Klienthemlighet"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "Conversion"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.actions.act_window,name:website_sale_dashboard.sale_dashboard
msgid "Dashboard"
msgstr "Anslagstavla"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.res_config_settings_view_form
msgid "How to get my Client ID"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.model.fields,field_description:website_sale_dashboard.field_sale_report__order_id
msgid "Order"
msgstr "Order"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "Orders"
msgstr "Ordrar"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.view_online_sales_graph
#: model:ir.ui.view,arch_db:website_sale_dashboard.view_online_sales_pivot
msgid "Sales Analysis"
msgstr "Försäljningsanalys"

#. module: website_sale_dashboard
#: model:ir.model,name:website_sale_dashboard.model_sale_report
msgid "Sales Orders Statistics"
msgstr "Kundorderstatistik"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.res_config_settings_view_form
msgid "See Google Analytics data on your Website Dashboard"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_view
msgid "Sold"
msgstr ""

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_search_view
msgid "This Month"
msgstr "Denna Månaden"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_search_view
msgid "This Week"
msgstr "Den här veckan"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_search_view
msgid "This Year"
msgstr "Det här året"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_search_view
msgid "Today"
msgstr "Idag"

#. module: website_sale_dashboard
#: model:ir.model,name:website_sale_dashboard.model_website
msgid "Website"
msgstr "Webbplats"

#. module: website_sale_dashboard
#: model:ir.ui.view,arch_db:website_sale_dashboard.sale_dashboard_search_view
msgid "e-Commerce Analysis"
msgstr ""
