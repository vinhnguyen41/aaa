# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * documents_sign
# 
# Translators:
# Jeffery CHEN Fan <jeffery9@gmail.com>, 2018
# ChinaMaker <liuct@chinamaker.net>, 2018
# liAnGjiA <liangjia@qq.com>, 2018
# xu xiaohu <xu.xiaohu@gmail.com>, 2018
# inspur qiuguodong <qiuguodong@inspur.com>, 2018
# neter ji <jifuyi@qq.com>, 2018
# Martin Trigaux, 2018
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-19 09:12+0000\n"
"PO-Revision-Date: 2018-08-24 11:37+0000\n"
"Last-Translator: Martin Trigaux, 2018\n"
"Language-Team: Chinese (China) (https://www.transifex.com/odoo/teams/41243/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_documents_workflow_rule
msgid ""
"A set of condition and actions which will be available to all attachments "
"matching the conditions"
msgstr "一组适用于符合所有条件附件的状态动作集合"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_request__folder_id
#: model:ir.model.fields,field_description:documents_sign.field_sign_template__folder_id
msgid "Attachment Folder"
msgstr "附件夹"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_request__documents_tag_ids
#: model:ir.model.fields,field_description:documents_sign.field_sign_template__documents_tag_ids
msgid "Attachment Tags"
msgstr "附件标签"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_documents_workflow_rule__create_model
msgid "Create"
msgstr "创建"

#. module: documents_sign
#: selection:documents.workflow.rule,create_model:0
msgid "Credit note"
msgstr "信用证"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_ir_attachment
msgid "Document"
msgstr "文档"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_documents_workflow_rule__has_business_option
msgid "Has Business Option"
msgstr "商业选项"

#. module: documents_sign
#: selection:documents.workflow.rule,create_model:0
msgid "Product template"
msgstr "产品模板"

#. module: documents_sign
#: model:documents.workflow.rule,name:documents_sign.Sign_rule
msgid "Request a Signature"
msgstr "请求签名"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_sign_request
msgid "Signature Request"
msgstr "签字需求"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_sign_template
msgid "Signature Template"
msgstr "签名模板"

#. module: documents_sign
#: selection:documents.workflow.rule,create_model:0
msgid "Signature template"
msgstr "签名模板"

#. module: documents_sign
#: selection:documents.workflow.rule,create_model:0
msgid "Task"
msgstr "任务"

#. module: documents_sign
#: selection:documents.workflow.rule,create_model:0
msgid "Vendor bill"
msgstr "供应商发票"
